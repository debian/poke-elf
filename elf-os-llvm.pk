/* elf-os-llvm.pk - ELF LLVM specific definitions.  */

/* Copyright (C) 2024 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* LLVM is not an OS, but it uses the OS-specific ranges for its ELF
   extensions.

   Values extracted from the include/llvm/BinaryFormat/ELF.h file in
   the LLVM source distribution.  */

/* LLVM section types.  */

var ELF_SHT_LLVM_ODRTAB = 0x6fff4c00U,
    ELF_SHT_LLVM_LINKER_OPTIONS = 0x6fff4c01U,
    ELF_SHT_LLVM_ADDRSIG = 0x6fff4c03U,
    ELF_SHT_LLVM_DEPENDENT_LIBRARIES = 0x6fff4c04U,
    ELF_SHT_LLVM_SYMPART = 0x6fff4c05U,
    ELF_SHT_LLVM_PART_EHDR = 0x6fff4c06U,
    ELF_SHT_LLVM_PART_PHDR = 0x6fff4c07U,
    ELF_SHT_LLVM_BB_ADDR_MAP_V0 = 0x6fff4c08U,
    ELF_SHT_LLVM_CALL_GRAPH_PROFILE = 0x6fff4c09U,
    ELF_SHT_LLVM_BB_ADDR_MAP = 0x6fff4c0aU,
    ELF_SHT_LLVM_OFFLOADING = 0x6fff4c0bU;

elf_config.add_enum
  :class "section-types"
  :entries [Elf_Config_UInt { value = ELF_SHT_LLVM_ODRTAB, name ="llvm-odrtab",
                              doc = "LLVM ODR table" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_LINKER_OPTIONS, name = "llvm-linker-options",
                              doc = "LLVM Linker Options" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_ADDRSIG, name = "llvm-addrsig",
                              doc = "List of address-significant symbols for safeICF" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_DEPENDENT_LIBRARIES, name = "llvm-dependent-libraries",
                              doc = "LLVM Dependend library specifiers" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_SYMPART, name = "llvm-sympart",
                              doc = "Symbol partition specification" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_PART_EHDR, name = "llvm-part-ehdr",
                              doc = "ELF header for loadable partition" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_PART_PHDR, name = "llvm-part-phdr",
                              doc = "Phdrs for loadable partition" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_BB_ADDR_MAP_V0, name = "llvm-bb-addr-map-v0",
                              doc = "LLVM basic block address map (old version)" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_CALL_GRAPH_PROFILE, name = "llvm-call-graph-profile",
                              doc = "LLVM call graph profile" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_BB_ADDR_MAP, name = "llvm-bb-addr-map",
                              doc = "LLVM basic block address map" },
            Elf_Config_UInt { value = ELF_SHT_LLVM_OFFLOADING, name = "llvm-offloading",
                              doc = "LLVM device offloading data" }];

/* Contents of sections of type ELF_SHT_LLVM_CALL_GRAPH_PROFILE.  */

type Elf_CGProfile =
  struct
  {
    Elf_Word cgp_from; /* Symbol index of the source of the edge.  */
    Elf_Word cgp_to;   /* Symbol index of the destination of the edge.  */
    uint<64> cgp_weight; /* The weight of the edge.  */
  };
