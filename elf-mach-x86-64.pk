/* elf--mach-x86-64.pk - ELF X86-64 specific definitions.  */

/* Copyright (C) 2024 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* X86-64 relocation types.  */

var ELF_R_X86_64_NONE = 0U,
    ELF_R_X86_64_64 = 1U,
    ELF_R_X86_64_PC32 = 2U,
    ELF_R_X86_64_GOT32 = 3U,
    ELF_R_X86_64_PLT32 = 4U,
    ELF_R_X86_64_COPY = 5U,
    ELF_R_X86_64_GLOB_DAT = 6U,
    ELF_R_X86_64_JUMP_SLOT = 7U,
    ELF_R_X86_64_RELATIVE = 8U,
    ELF_R_X86_64_GOTPCREL = 9U,
    ELF_R_X86_64_32 = 10U,
    ELF_R_X86_64_32S = 11U,
    ELF_R_X86_64_16 = 12U,
    ELF_R_X86_64_PC16 = 13U,
    ELF_R_X86_64_8 = 14U,
    ELF_R_X86_64_PC8 = 15U,
    ELF_R_X86_64_DTPMOD64 = 16U,
    ELF_R_X86_64_DTPOFF64 = 17U,
    ELF_R_X86_64_TPOFF64 = 18U,
    ELF_R_X86_64_TLSGD = 19U,
    ELF_R_X86_64_TLSLD = 20U,
    ELF_R_X86_64_DTPOFF32 = 21U,
    ELF_R_X86_64_GOTTPOFF = 22U,
    ELF_R_X86_64_TPOFF32 = 23U,
    ELF_R_X86_64_PC64 = 24U,
    ELF_R_X86_64_GOTOFF64 = 25U,
    ELF_R_X86_64_GOTPC32 = 26U,
    ELF_R_X86_64_GOT64 = 27U,
    ELF_R_X86_64_GOTPCREL64 = 28U,
    ELF_R_X86_64_GOTPC64 = 29U,
    ELF_R_X86_64_GOTPLT64 = 30U,
    ELF_R_X86_64_PLTOFF64 = 31U,
    ELF_R_X86_64_SIZE32 = 32U,
    ELF_R_X86_64_SIZE64 = 33U,
    ELF_R_X86_64_GOTPC32_TLSDESC = 34U,
    ELF_R_X86_64_TLSDESC_CALL = 35U,
    ELF_R_X86_64_TLSDESC = 36U,
    ELF_R_X86_64_IRELATIVE = 37U,
    ELF_R_X86_64_RELATIVE64 = 38U,
    ELF_R_X86_64_PC32_BND = 39U,
    ELF_R_X86_64_PLT32_BND = 40U,
    ELF_R_X86_64_GOTPCRELX = 41U,
    ELF_R_X86_64_REX_GOTPCRELX = 42U,
    ELF_R_X86_64_GNU_VTINHERIT = 250U,  /* GNU C++ hack  */
    ELF_R_X86_64_GNU_VTENTRY = 251U;    /* GNU C++ hack  */

elf_config.add_enum
  :machine ELF_EM_X86_64
  :class "reloc-types"
  :entries [Elf_Config_UInt { value = ELF_R_X86_64_NONE, name = "none",
                              doc = "No reloc." },
            Elf_Config_UInt { value = ELF_R_X86_64_64, name = "64",
                              doc = "Direct 64 bit." },
            Elf_Config_UInt { value = ELF_R_X86_64_PC32, name = "pc32",
                              doc = "PC relative 32 bit signed." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOT32, name = "got32",
                              doc = "32 bit GOT entry." },
            Elf_Config_UInt { value = ELF_R_X86_64_PLT32, name = "plt32",
                              doc = "32 bit PLT address." },
            Elf_Config_UInt { value = ELF_R_X86_64_COPY, name = "copy",
                              doc = "Copy symbol at runtime." },
            Elf_Config_UInt { value = ELF_R_X86_64_GLOB_DAT, name = "glob-dat",
                              doc = "Create GOT entry." },
            Elf_Config_UInt { value = ELF_R_X86_64_JUMP_SLOT, name = "jump-slot",
                              doc = "Create PLT entry." },
            Elf_Config_UInt { value = ELF_R_X86_64_RELATIVE, name = "relative",
                              doc = "Adjust by program base." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTPCREL, name = "gotpcrel",
                              doc = "32 bit signed pc relative offset to GOT entry." },
            Elf_Config_UInt { value = ELF_R_X86_64_32, name = "32",
                              doc = "Direct 32 bit zero extended." },
            Elf_Config_UInt { value = ELF_R_X86_64_32S, name = "32S",
                              doc = "Direct 32 bit sign extended." },
            Elf_Config_UInt { value = ELF_R_X86_64_16, name = "16",
                              doc = "Direct 16 bit zero extended." },
            Elf_Config_UInt { value = ELF_R_X86_64_PC16, name = "pc16",
                              doc = "16 bit sign extended pc relative." },
            Elf_Config_UInt { value = ELF_R_X86_64_8, name = "8",
                              doc = "Direct 8 bit zero extended." },
            Elf_Config_UInt { value = ELF_R_X86_64_PC8, name = "pc8",
                              doc = "8 bit sign extended pc relative." },
            Elf_Config_UInt { value = ELF_R_X86_64_DTPMOD64, name = "dtpmod64",
                              doc = "ID of module containing symbol." },
            Elf_Config_UInt { value = ELF_R_X86_64_DTPOFF64, name = "dtpoff64",
                              doc = "Offset in TLS block." },
            Elf_Config_UInt { value = ELF_R_X86_64_TPOFF64, name = "tpoff64",
                              doc = "Offset in initial TLS block." },
            Elf_Config_UInt { value = ELF_R_X86_64_TLSGD, name = "tlsgd",
                              doc = "PC relative offset to GD GOT block." },
            Elf_Config_UInt { value = ELF_R_X86_64_TLSLD, name = "tlsld",
                              doc = "PC relative offset to LD GOT block." },
            Elf_Config_UInt { value = ELF_R_X86_64_DTPOFF32, name = "dtpoff32",
                              doc = "Offset in TLS block." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTTPOFF, name = "gottpoff",
                              doc = "PC relative offset to IE GOT entry." },
            Elf_Config_UInt { value = ELF_R_X86_64_TPOFF32, name = "tpoff32",
                              doc = "Offset in initial TLS block." },
            Elf_Config_UInt { value = ELF_R_X86_64_PC64, name = "pc64",
                              doc = "PC relative 64 bit." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTOFF64, name = "gotoff64",
                              doc = "64 bit offset to GOT." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTPC32, name = "gotpc32",
                              doc = "32 bit signed pc relative offset to GOT." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOT64, name = "got64",
                              doc = "64 bit GOT entry offset." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTPCREL64, name = "gotpcrel64",
                              doc = "64 bit signed pc relative offset to GOT entry." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTPC64, name = "gotpc64",
                              doc = "64 bit signed pc relative offset to GOT." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTPLT64, name = "gotplt64",
                              doc = "Obsolete.  The same as got64." },
            Elf_Config_UInt { value = ELF_R_X86_64_PLTOFF64, name = "pltoff64",
                              doc = "64-bit GOT relative offset to PLT entry." },
            Elf_Config_UInt { value = ELF_R_X86_64_SIZE32, name = "size32",
                              doc = "32-bit symbol size." },
            Elf_Config_UInt { value = ELF_R_X86_64_SIZE64, name = "size64",
                              doc = "64-bit symbol size." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTPC32_TLSDESC, name = "gotpc32-tlsdesc",
                              doc = "32 bit signed pc relative offset to TLS descriptor in the GOT." },
            Elf_Config_UInt { value = ELF_R_X86_64_TLSDESC_CALL, name = "tlsdec-call",
                              doc = "Relaxable call through TLS descriptor." },
            Elf_Config_UInt { value = ELF_R_X86_64_TLSDESC, name = "tlsdesc",
                              doc = "2x64-bit TLS descriptor." },
            Elf_Config_UInt { value = ELF_R_X86_64_IRELATIVE, name = "irelative",
                              doc = "Adjust indirectly by program base." },
            Elf_Config_UInt { value = ELF_R_X86_64_RELATIVE64, name = "relative64",
                              doc = "64 bit adjust by program base." },
            Elf_Config_UInt { value = ELF_R_X86_64_PC32_BND, name = "pc32bnd",
                              doc = "PC relative 32 bit signed with BND prefix." },
            Elf_Config_UInt { value = ELF_R_X86_64_PLT32_BND, name = "plt32bnd",
                              doc = "32 bit PLT address with BND prefix." },
            Elf_Config_UInt { value = ELF_R_X86_64_GOTPCRELX, name = "gotpcrelx",
                              doc = "Load from 32 bit signed pc relative offset to "
                                    + "GOT entry without REX prefix, relaxable." },
            Elf_Config_UInt { value = ELF_R_X86_64_REX_GOTPCRELX, name = "rex-gotpcrelx",
                              doc = "Load from 32 bit signed pc relative offset to "
                                    + "GOT entry with REX prefix, relaxable." },
            Elf_Config_UInt { value = ELF_R_X86_64_GNU_VTINHERIT, name = "gnu-vtinherit",
                              doc = "GNU C++ hack." },
            Elf_Config_UInt { value = ELF_R_X86_64_GNU_VTENTRY, name = "gnu-vtentry",
                              doc = "GNU C++ hack." }];

/* X86_64 section types.  */

var ELF_SHT_X86_64_UNWIND = 0x70000001U; /* unwind information */

elf_config.add_enum
  :machine ELF_EM_X86_64
  :class "section-types"
  :entries [Elf_Config_UInt { value = ELF_SHT_X86_64_UNWIND, name = "unwind",
                              doc = "Section contains unwind info." }];

/* X86_64 section flags.  */

var ELF_SHF_X86_64_LARGE = 0x10000000UL;

elf_config.add_mask
  :machine ELF_EM_X86_64
  :class "section-flags"
  :entries [Elf_Config_Mask { value = ELF_SHF_X86_64_LARGE, name = "large" }];
