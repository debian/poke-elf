/* elf-mach-bpf.pk - ELF BPF specific definitions.  */

/* Copyright (C) 2024 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* BPF relocation types.  */

var ELF_R_BPF_NONE = 0U,
    ELF_R_BPF_64_64 = 1U,
    ELF_R_BPF_64_ABS64 = 2U,
    ELF_R_BPF_64_ABS32 = 3U,
    ELF_R_BPF_64_NODYLD32 = 4U,
    ELF_R_BPF_64_32 = 10U;

elf_config.add_enum
  :machine ELF_EM_BPF
  :class "reloc-types"
  :entries [Elf_Config_UInt { value = ELF_R_BPF_NONE, name = "none" },
            Elf_Config_UInt { value = ELF_R_BPF_64_64, name = "64" },
            Elf_Config_UInt { value = ELF_R_BPF_64_ABS64, name = "abs64" },
            Elf_Config_UInt { value = ELF_R_BPF_64_ABS32, name = "abs32" },
            Elf_Config_UInt { value = ELF_R_BPF_64_NODYLD32, name = "nodyld32" },
            Elf_Config_UInt { value = ELF_R_BPF_64_32, name = "32" }];
