/* elf-os-openbsd.pk - ELF OpenBSD specific definitions.  */

/* Copyright (C) 2024 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* OPENBSD segment types.  */

var ELF_PT_OPENBSD_MUTABLE = ELF_PT_LOOS + 0x5a3dbe5,
    ELF_PT_OPENBSD_RANDOMIZE = ELF_PT_LOOS + 0x5a3dbe6,
    ELF_PT_OPENBSD_WXNEEDED = ELF_PT_LOOS + 0x5a3dbe7,
    ELF_PT_OPENBSD_NOBTCFI = ELF_PT_LOOS + 0x5a3dbe8,
    ELF_PT_OPENBSD_BOOTDATA = ELF_PT_LOOS + 0x5a41be6;

elf_config.add_enum
  :class "segment-types"
  :entries [Elf_Config_UInt { value = ELF_PT_OPENBSD_MUTABLE, name = "openbsd-mutable",
                              doc = "Like bss, but not immutable." },
            Elf_Config_UInt { value = ELF_PT_OPENBSD_RANDOMIZE, name = "openbsd-randomize",
                              doc = "Fill with random data." },
            Elf_Config_UInt { value = ELF_PT_OPENBSD_WXNEEDED, name = "openbsd-wxneeded",
                              doc = "Program does W^X violations." },
            Elf_Config_UInt { value = ELF_PT_OPENBSD_NOBTCFI, name = "openbsd-nobtcfi",
                              doc = "No branch target CFI." },
            Elf_Config_UInt { value = ELF_PT_OPENBSD_BOOTDATA, name = "openbsd-bootdata",
                              doc = "Section for bot arguments." }];

/* Note segments for core files on OpenBSD systems.  */

var ELF_NT_OPENBSD_IDENT = 1,
    ELF_NT_OPENBSD_PROCINFO = 10,
    ELF_NT_OPENBSD_AUXV = 11,
    ELF_NT_OPENBSD_REGS = 20,
    ELF_NT_OPENBSD_FPREGS = 21,
    ELF_NT_OPENBSD_XFPREGS = 22,
    ELF_NT_OPENBSD_WCOOKIE = 23;

elf_config.add_enum
  :class "note-tags"
  :entries [Elf_Config_UInt { value = ELF_NT_OPENBSD_IDENT, name = "openbsd-ident" },
            Elf_Config_UInt { value = ELF_NT_OPENBSD_PROCINFO, name = "openbsd-procinfo" },
            Elf_Config_UInt { value = ELF_NT_OPENBSD_AUXV, name = "openbsd-auxv" },
            Elf_Config_UInt { value = ELF_NT_OPENBSD_REGS, name = "openbsd-regs" },
            Elf_Config_UInt { value = ELF_NT_OPENBSD_FPREGS, name = "openbsd-fpregs" },
            Elf_Config_UInt { value = ELF_NT_OPENBSD_XFPREGS, name = "openbsd-xfpregs" },
            Elf_Config_UInt { value = ELF_NT_OPENBSD_WCOOKIE, name = "openbsd-wcookie" }];
