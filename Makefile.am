# ELF pickle for GNU poke

# Copyright (C) 2024 Jose E. Marchesi

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

picklesdir = $(datadir)/poke/pickles
dist_pickles_DATA = elf.pk \
                    elf-build.pk \
                    elf-common.pk \
                    elf-64.pk elf-32.pk \
                    elf-config.pk \
                    elf-mach-x86-64.pk \
                    elf-mach-aarch64.pk \
                    elf-mach-mips.pk \
                    elf-mach-sparc.pk \
                    elf-mach-bpf.pk \
                    elf-mach-riscv.pk \
                    elf-mach-arm.pk \
                    elf-os-gnu.pk \
                    elf-os-llvm.pk \
                    elf-os-linux.pk \
                    elf-os-openbsd.pk

info_TEXINFOS = poke-elf.texi
AM_MAKEINFOFLAGS = --set-customization-variable CHECK_NORMAL_MENU_STRUCTURE=true
EXTRA_DIST = elf-build.pk.in

# Example utilities.

dist_bin_SCRIPTS = examples/prelinkr

# Running the testsuite.
#
# Note that it is important to invoke POKE from the same directory
# where the pickle's .pk files are, so they are picked up before the
# system-installed pickles.

EXTRA_DIST += test-elf.pk.in

if RUN_TESTS
TEST_LOG_DRIVER = env AM_TAP_AWK='$(AWK)' \
                  $(SHELL) $(top_srcdir)/build-aux/tap-driver.sh
TESTS_ENVIRONMENT = POKE_LOAD_PATH=$(top_srcdir)
TESTS = test-elf.pk
endif
